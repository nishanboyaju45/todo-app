Rails.application.routes.draw do
  resources :todos # Crud resource
  # get 'home/index'
  get 'home/about'
  root 'todos#index'
  # root 'home#index'


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
